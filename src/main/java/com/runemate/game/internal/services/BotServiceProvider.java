package com.runemate.game.internal.services;

import com.runemate.client.framework.open.*;
import com.runemate.game.api.script.framework.*;
import java.util.*;
import java.util.concurrent.*;

public class BotServiceProvider implements BotServicesProvider {

    private static final Map<AbstractBot, BotService> services = new ConcurrentHashMap<>();

    @Override
    public BotService getService(AbstractBot abstractBot) {
        return services.computeIfAbsent(abstractBot, BotServiceImpl::new);
    }
}
