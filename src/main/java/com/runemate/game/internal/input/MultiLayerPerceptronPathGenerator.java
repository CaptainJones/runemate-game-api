package com.runemate.game.internal.input;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.script.*;
import com.runemate.game.internal.input.mlp.loading.*;
import com.runemate.game.internal.input.mlp.preprocessing.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import org.jblas.*;

public class MultiLayerPerceptronPathGenerator extends Mouse.PathGenerator {
    private MultilayerPerceptron model;
    private MinMaxScalar xScalar, yScalar;

    private boolean initialized = false;

    public void init(InputStream weights, InputStream model, InputStream xScalar, InputStream yScalar) {
        try {
            // Create the model
            this.model = new KerasLoader(weights, model).buildModel();
            // Create the scalars
            this.xScalar = new MinMaxScalar(xScalar, "x");
            this.yScalar = new MinMaxScalar(yScalar, "y");
            initialized = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean move(Interactable interactable, double velocityMultiplier) {
        return move(interactable, velocityMultiplier, 0);
    }

    private boolean move(Interactable interactable, double velocityMultiplier, int previousTries) {
        if (!initialized) {
            try {
                init(OpenResources.getAsStream(OpenResource.MLP_WEIGHTS),
                    OpenResources.getAsStream(OpenResource.MLP_MODEL),
                    OpenResources.getAsStream(OpenResource.MLP_SCALE),
                    OpenResources.getAsStream(OpenResource.MLP_SCALE));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //TODO player sense the 10 try timeout.
        if (model == null || previousTries > 10) {
            return false;
        }
        Point target = interactable.getInteractionPoint();
        if (target == null) {
            return false;
        }
        double distanceTraveled = 0; // The sum of the distance of every hop
        long timeElapsed = 0; // The sum of the ms of every hop
        double velocity = 0; // distanceTraveled / timeElapsed
        int sleepMs = 0; // Previous sleep time
        double distanceDelta = Double.MAX_VALUE; // How much has the distance changed since last hop
        // Grab the start and end
        Point position = Mouse.getPosition();
        while (this.distance(position, target) > 4) {
            long startTime = System.currentTimeMillis();
            // If this distance delta is small and we're still not near the destination cut the distanceTraveled in half
            // and it will "renew" the path and cause it to keep going
            if (distanceDelta < 1) {
                distanceTraveled = distanceTraveled / 2;
            }
            // Update running values
            double tmpVelocity = sleepMs == 0 ? 0 : distanceTraveled / timeElapsed;
            double acceleration = sleepMs == 0 ? 0 : (tmpVelocity - velocity) / sleepMs;
            velocity = tmpVelocity;
            double angleTo = this.angle(position, target); // angle from current point to end point
            double distanceTo =
                this.distance(position, target); // distance from current point to end point
            // Prepare the data for the neural network
            DoubleMatrix input = new DoubleMatrix(
                new double[][] { { velocity, acceleration, angleTo, distanceTo, distanceTraveled } });
            // Scale it
            this.xScalar.mutateTransform(input);
            // Make prediction
            DoubleMatrix output = this.model.predict(input);
            // Inverse scale the prediction
            this.yScalar.mutateInverseTransform(output);
            // Round the outputs of the network
            int nextX = (int) Math.ceil(output.get(0, 0));
            int nextY = (int) Math.ceil(output.get(1, 0));
            int sleep = (int) Math.round(output.get(2, 0));
            // Add the predicted values to the current one
            final Point next = new Point(nextX + position.x, nextY + position.y);
            distanceDelta = this.distance(position, next);
            distanceTraveled += distanceDelta;
            // Move the mouse
            Mouse.PathGenerator.hop(next);
            // Calculate sleep time
            long sleepTime = Math.max(0, (sleep - (System.currentTimeMillis() - startTime)));
            // Apply the velocity modifier and sleep
            Execution.delay((long) (sleepTime * (1 / velocityMultiplier)));
            // Update current position
            position = Mouse.getPosition();
            // Update our running values
            timeElapsed += sleepTime;
            sleepMs = (int) sleepTime;
        }
        boolean hovered = interactable.isHovered();
        if (hovered) {
            Environment.getLogger().debug("Hovered " + interactable + " on the first attempt.");
            return true;
        }
        // If we're close enough just set the position and move, jagex only grab the position every 50 ms
        if (this.distance(position, target) <= 4) {
            Environment.getLogger().debug(
                "Attempting to hover " + interactable + " since the target of " + target
                    + " is less then 4 pixels away.");
            Mouse.PathGenerator.hop(target);
            Execution.delayUntilCycle(RuneScape.getCurrentCycle() + 2);
            hovered = interactable.isHovered();
            Environment.getLogger().debug("Attempted to hover " + interactable + " since " + target
                + " was less than 4 pixels away (success: " + hovered + ")");
            if (hovered) {
                return true;
            }
        } else {
            hovered = interactable.isHovered();
            if (hovered) {
                Environment.getLogger().debug(
                    "We're hovering " + interactable + " although our distance to " + target
                        + " is " + distance(position, target));
                return true;
            } else {
                return move(interactable, velocityMultiplier, previousTries + 1);
            }
        }
        if (Objects.equals(position, target)) {
            Environment.getLogger().debug("We're not hovering " + interactable
                + " according to Interactable#isHovered() but our position does equal the desired target.");
            return true;
        }
        return false;
    }

    private double angle(final Point p1, final Point p2) {
        return (Math.toDegrees(Math.atan2(p2.y - p1.y, p2.x - p1.x)) + 360) % 360;
    }

    private double distance(final Point p1, final Point p2) {
        double x = p2.x - p1.x;
        double y = p2.y - p1.y;
        return Math.sqrt((x * x) + (y * y));
    }
}