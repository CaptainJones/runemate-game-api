package com.runemate.game.internal.input.mlp.activation.impl;

import com.runemate.game.internal.input.mlp.activation.ActivationFunction;


/**
 * Rectified linear unit
 *
 * f(x) = max(0, x)
 */
public class Relu implements ActivationFunction {


    /**
     * @inheritdoc
     */
    @Override
    public double activate(double x) {
        return Math.max(0, x);
    }


    /**
     * @inheritdoc
     */
    @Override
    public String getIdentifier() {
        return "relu";
    }

}
