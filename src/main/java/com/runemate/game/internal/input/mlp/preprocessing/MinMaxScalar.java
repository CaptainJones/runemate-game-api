package com.runemate.game.internal.input.mlp.preprocessing;

import org.jblas.DoubleMatrix;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class MinMaxScalar {

    private final List<FeatureScalar> featureScalars = new ArrayList<>();

    public MinMaxScalar(final InputStream pythonOutfile, final String key) {
        try {
            final String fileContents = this.readStream(pythonOutfile);

            final JSONObject contents = new JSONObject(fileContents);
            final JSONArray xScalars = contents.getJSONArray(key);

            for (int i = 0; i < xScalars.length(); i++) {
                final JSONObject featureScalar = xScalars.getJSONObject(i);

                final double dataMax = featureScalar.getDouble("data_max");
                final double dataMin = featureScalar.getDouble("data_min");
                final double dataRange = featureScalar.getDouble("data_range");
                final double min = featureScalar.getDouble("min");
                final double scale = featureScalar.getDouble("scale");

                this.featureScalars.add(new FeatureScalar(dataMax, dataMin, dataRange, min, scale));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String readStream(final InputStream in) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }

        return out.toString();
    }

    public void mutateTransform(final DoubleMatrix matrix) {

        if (matrix.columns != this.featureScalars.size()) {
            System.out.println("Matrix does not fit the size of the feature scalars");
            return;
        }

        for (int i = 0; i < matrix.rows; i++) {
            for (int j = 0; j < matrix.columns; j++) {

                final FeatureScalar scalar = this.featureScalars.get(j);

                double item = matrix.get(i, j);
                item *= scalar.getScale();
                item += scalar.getMin();
                matrix.put(i, j, item);
            }
        }
    }


    public void mutateInverseTransform(final DoubleMatrix matrix) {

        if (matrix.rows != this.featureScalars.size()) {
            System.out.println("Matrix does not fit the size of the feature scalars");
            return;
        }

        for (int i = 0; i < matrix.rows; i++) {

            final FeatureScalar scalar = this.featureScalars.get(i);

            for (int j = 0; j < matrix.columns; j++) {
                double item = matrix.get(i, j);
                item -= scalar.getMin();
                item /= scalar.getScale();
                matrix.put(i, j, item);
            }
        }
    }
}
