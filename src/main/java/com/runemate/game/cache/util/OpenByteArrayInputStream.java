package com.runemate.game.cache.util;

import java.io.*;
import java.util.*;

public class OpenByteArrayInputStream extends ByteArrayInputStream {
    public OpenByteArrayInputStream(byte[] buf) {
        super(buf);
    }

    public int size() {
        return buf.length;
    }

    public int position() {
        return pos;
    }

    public void position(int position) {
        pos = position;
    }

    /**
     * Reads a byte
     */
    public byte peak() throws EOFException {
        if (pos >= buf.length) {
            throw new EOFException();
        }
        return buf[pos];
    }

    public byte[] getBackingArray() {
        return buf;
    }

    public byte[] getCopyOfBackingArray() {
        return Arrays.copyOf(buf, buf.length);
    }
}
