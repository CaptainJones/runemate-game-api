package com.runemate.game.cache.file;

import com.google.common.primitives.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.util.Bytes;
import com.runemate.game.cache.util.*;
import java.io.*;
import java.nio.*;
import java.util.*;
import java.util.concurrent.atomic.*;
import java.util.zip.*;

public class ReferenceTable {
    private final Js5IndexFile file;
    private final AtomicBoolean initialized;
    private int version;
    private int[] groupNames;
    private int groupCount;
    private int[] groupIndices;
    private int groupSize;
    private int[] groupCrcs;
    private HashedNameLookupTable groupsNameTable;
    private byte[][] groupHashes;
    private int[] anIntArray4626;
    private int[] groupVersions;
    private int[] fileCounts;
    private int[] anIntArray4624;
    private int[] anIntArray4630;
    private int[][] fileIndices;
    private int[] fileSizes;
    private int[][] fileNameHashes;
    private HashedNameLookupTable[] filesNameTables;

    public ReferenceTable(Js5IndexFile file) {
        this.file = file;
        this.initialized = new AtomicBoolean(false);
    }

    public int getIndexOfGroupHash(int hash) {
        if (!file.hasLoaded()) {
            file.load();
        }
        return groupsNameTable.getIndex(hash);
    }

    public int getIndexOfFileHash(int group, int hash) {
        return filesNameTables[group].getIndex(hash);
    }

    public synchronized void initialize(Js5IndexFile meta, Js5Dat2File cache) throws IOException {
        if (!initialized()) {
            int key = file.archiveIndex();
            byte[] chunk = readChunk(cache, meta.getInitialChunksInfo().get(key));
            byte[] data = Bytes.decompress(chunk, null);
            decode(data);
            initialized.set(true);
        }
    }

    public boolean initialized() {
        return initialized.get();
    }

    public byte[] readChunk(Js5Dat2File cache, InitialChunkInfo chunk) throws IOException {
        ByteBuffer data = ByteBuffer.allocate(chunk.getSize());
        Js5Dat2File.Chunk unit = cache.readChunk(chunk);
        while (unit != null) {
            byte[] bytes = unit.getContents();
            int offset = unit.getOffset();
            data.put(bytes, offset, Math.min(bytes.length - offset, data.remaining()));
            unit = unit.getNext(chunk);
        }
        return data.array();
    }

    public void decode(byte[] data) {
        Js5InputStream buffer = new Js5InputStream(data);
        try {
            int protocol = buffer.readUnsignedByte();
            if (protocol < 5 || protocol > 7) {
                throw new RuntimeException("Unknown reference table version");
            }
            if (protocol >= 6) {
                version = buffer.readInt();
            }
            int attributes = buffer.readUnsignedByte();
            boolean named = (attributes & 0x1) != 0;
            boolean hashed = (attributes & 0x2) != 0;
            boolean bool_5_ = (attributes & 0x4) != 0;
            boolean bool_6_ = (attributes & 0x8) != 0;

            if (protocol >= 7) {
                groupCount = buffer.readUnsignedSmart();
            } else {
                groupCount = buffer.readUnsignedShort();
            }

            int offset = 0;
            int max = -1;
            groupIndices = new int[groupCount];

            if (protocol >= 7) {
                for (int entry = 0; entry < groupCount; entry++) {
                    groupIndices[entry] = offset += buffer.readUnsignedSmart();
                    if (groupIndices[entry] > max) {
                        max = groupIndices[entry];
                    }
                }
            } else {
                for (int entry = 0; entry < groupCount; entry++) {
                    groupIndices[entry] = offset += buffer.readUnsignedShort();
                    if (groupIndices[entry] > max) {
                        max = groupIndices[entry];
                    }
                }
            }

            groupSize = (max + 1);
            groupCrcs = new int[groupSize];
            if (bool_6_) {
                anIntArray4626 = new int[groupSize];
            }
            if (hashed) {
                groupHashes = new byte[groupSize][];
            }
            groupVersions = new int[groupSize];
            fileCounts = new int[groupSize];
            fileIndices = new int[groupSize][];
            fileSizes = new int[groupSize];
            if (named) {
                groupNames = new int[groupSize];
                for (int index = 0; index < groupSize; index++) {
                    groupNames[index] = -1;
                }
                for (int index = 0; index < groupCount; index++) {
                    groupNames[groupIndices[index]] = buffer.readInt();
                }
                groupsNameTable = new HashedNameLookupTable(groupNames);
            }
            for (int index = 0; index < groupCount; index++) {
                groupCrcs[groupIndices[index]] = buffer.readInt();
            }
            if (bool_6_) {
                for (int index = 0; index < groupCount; index++) {
                    anIntArray4626[index] = buffer.readInt();
                }
            }
            if (hashed) {
                for (int index = 0; index < groupCount; index++) {
                    byte[] hash = new byte[64];
                    if (buffer.read(hash, 0, 64) != 64) {
                        throw new EOFException("Not all of the hashes bytes were read.");
                    }
                    groupHashes[groupIndices[index]] = hash;
                }
            }
            if (bool_5_) {
                anIntArray4624 = new int[groupSize];
                anIntArray4630 = new int[groupSize];
                for (int index = 0; index < groupCount; index++) {
                    anIntArray4624[groupIndices[index]] = buffer.readInt();
                    anIntArray4630[groupIndices[index]] = buffer.readInt();
                }
            }
            for (int index = 0; index < groupCount; index++) {
                groupVersions[groupIndices[index]] = buffer.readInt();
            }
            int avar;
            if (protocol >= 7) {
                for (int index = 0; index < groupCount; index++) {
                    fileCounts[groupIndices[index]] = buffer.readUnsignedSmart();
                }
                for (int group = 0; group < groupCount; group++) {
                    int groupIndex = groupIndices[group];
                    int count = fileCounts[groupIndex];
                    offset = 0;
                    int maxIndex = -1;
                    fileIndices[groupIndex] = new int[count];

                    for (int index = 0; index < count; index++) {
                        avar =
                            fileIndices[groupIndex][index] = offset += buffer.readUnsignedSmart();
                        if (avar > maxIndex) {
                            maxIndex = avar;
                        }
                    }

                    fileSizes[groupIndex] = maxIndex + 1;
                    if (1 + maxIndex == count) {
                        fileIndices[groupIndex] = null;
                    }
                }
            } else {
                for (int index = 0; index < groupCount; index++) {
                    fileCounts[groupIndices[index]] = buffer.readUnsignedShort();
                }

                for (int group = 0; group < groupCount; group++) {
                    int groupIndex = groupIndices[group];
                    int count = fileCounts[groupIndex];
                    offset = 0;
                    int maxIndex = -1;
                    fileIndices[groupIndex] = new int[count];
                    for (int child = 0; child < count; child++) {
                        avar =
                            fileIndices[groupIndex][child] = offset += buffer.readUnsignedShort();
                        if (avar > maxIndex) {
                            maxIndex = avar;
                        }
                    }

                    fileSizes[groupIndex] = maxIndex + 1;
                    if (1 + maxIndex == count) {
                        fileIndices[groupIndex] = null;
                    }
                }
            }

            if (named) {
                fileNameHashes = new int[1 + max][];
                filesNameTables = new HashedNameLookupTable[max + 1];

                for (int entry = 0; entry < groupCount; entry++) {
                    int groupIndex = groupIndices[entry];
                    int count = fileCounts[groupIndex];
                    fileNameHashes[groupIndex] = new int[fileSizes[groupIndex]];

                    for (int index = 0; index < fileSizes[groupIndex]; index++) {
                        fileNameHashes[groupIndex][index] = -1;
                    }

                    for (int index = 0; index < count; index++) {
                        int child;
                        if (fileIndices[groupIndex] != null) {
                            child = fileIndices[groupIndex][index];
                        } else {
                            child = index;
                        }
                        fileNameHashes[groupIndex][child] = buffer.readInt();
                    }
                    filesNameTables[groupIndex] =
                        new HashedNameLookupTable(fileNameHashes[groupIndex]);
                }
            }
        } catch (IOException e) {
            System.out.println("Unable to decode reference table.");
            e.printStackTrace();
        }
    }

    public int[] groups() {
        if (!file.hasLoaded()) {
            file.load();
        }
        return groupIndices;
    }

    public boolean exists(int group) {
        if (!file.hasLoaded()) {
            file.load();
        }
        InitialChunkInfo query = file.getInitialChunksInfo().get(group);
        if (query == null) {
            return false;
        }
        byte[] data;
        try {
            data = file.readChunk(query);
        } catch (BufferOverflowException | IOException boe) {
            return false;
        }
        if (data.length < 2 || data.length != query.getSize()) {
            //System.out.println("Incorrect query size for " + this.file.archiveIndex() + " " + group + ". Expected " + query.getSize() + " but actual is " + data.length);
            return false;
        }
        CRC32 crc32 = new CRC32();
        crc32.update(data, 0, data.length - 2);
        if (crc32.getValue() != UnsignedInts.toLong(groupCrcs[group])) {
            return false;
        }
        int version = ((data[data.length - 2] & 0xFF) << 8) + (data[data.length - 1] & 0xFF);
        return version == (groupVersions[group] & 0xFFFF);
    }

    public Map<Integer, byte[]> load(String group) {
        return load(DJB2Hasher.getGroupId(this, group));
    }

    public Map<Integer, byte[]> load(int group) {
        return load(group, null);
    }

    public Map<Integer, byte[]> load(int group, int[] xtea_keys) {
        if (!file.hasLoaded()) {
            file.load();
        }
        InitialChunkInfo query = file.getInitialChunksInfo().get(group);
        if (query == null) {
            return null;
        }
        byte[] data;
        try {
            data = file.readChunk(query);
        } catch (BufferOverflowException | IOException boe) {
            return null;
        }
        if (data.length < 2 || data.length != query.getSize()) {
            //System.out.println("Incorrect query size for " + this.file.archiveIndex() + " " + group + ". Expected " + query.getSize() + " but actual is " + data.length);
            return null;
        }
        CRC32 crc32 = new CRC32();
        crc32.update(data, 0, data.length - 2);
        if (crc32.getValue() != UnsignedInts.toLong(groupCrcs[group])) {
            return null;
        }
        int version = ((data[data.length - 2] & 0xFF) << 8) + (data[data.length - 1] & 0xFF);
        if (version != (groupVersions[group] & 0xFFFF)) {
            return null;
        }
        Map<Integer, byte[]> groups = new HashMap<>();
        new ArchiveGroupDecoder(fileCounts[group], fileIndices[group]).decode(groups, data,
            xtea_keys
        );
        return groups;
    }

    public Map<Integer, byte[]> loadLandscape(int group, int[] xtea_keys) {
        if (!file.hasLoaded()) {
            file.load();
        }
        InitialChunkInfo query = file.getInitialChunksInfo().get(group);
        if (query == null) {
            return null;
        }
        byte[] data;
        try {
            data = file.readChunk(query);
        } catch (BufferOverflowException | IOException boe) {
            return null;
        }
        if (data.length < 2 || data.length != query.getSize()) {
            //System.out.println("Incorrect query size for " + this.file.archiveIndex() + " " + group + ". Expected " + query.getSize() + " but actual is " + data.length);
            return null;
        }
        CRC32 crc32 = new CRC32();
        crc32.update(data, 0, data.length - 2);
        if (crc32.getValue() != UnsignedInts.toLong(groupCrcs[group])) {
            return null;
        }
        int version = ((data[data.length - 2] & 0xFF) << 8) + (data[data.length - 1] & 0xFF);
        if (version != (groupVersions[group] & 0xFFFF)) {
            return null;
        }
        if (data.length == 32) {
            throw new EmptyLandscapeException();
        }
        Map<Integer, byte[]> groups = new HashMap<>();
        new ArchiveGroupDecoder(fileCounts[group], fileIndices[group]).decode(groups, data,
            xtea_keys
        );
        return groups;
    }

    public Map<Integer, byte[]> load(String group, int[] xtea_keys) {
        return load(DJB2Hasher.getGroupId(this, group), xtea_keys);
    }
}
