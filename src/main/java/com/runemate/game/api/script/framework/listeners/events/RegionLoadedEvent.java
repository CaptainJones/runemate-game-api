package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.location.*;
import javax.annotation.*;

public class RegionLoadedEvent implements Event {

    private final Coordinate previousBase;
    private final Coordinate currentBase;

    public RegionLoadedEvent(@Nullable Coordinate previousBase, @Nonnull Coordinate currentBase) {
        this.previousBase = previousBase;
        this.currentBase = currentBase;
    }

    @Nullable
    public Coordinate getPreviousBase() {
        return previousBase;
    }

    @Nonnull
    public Coordinate getCurrentBase() {
        return currentBase;
    }

    @Override
    public String toString() {
        return "RegionLoadedEvent(previous=" + getPreviousBase() + ", current=" + getCurrentBase()
            + ")";
    }
}
