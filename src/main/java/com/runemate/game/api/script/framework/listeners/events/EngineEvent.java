package com.runemate.game.api.script.framework.listeners.events;

public class EngineEvent implements Event {
    private final Type type;

    public EngineEvent(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "EngineEvent{type=" + type + '}';
    }

    public enum Type {
        CLIENT_CYCLE,
        SERVER_TICK
    }
}
