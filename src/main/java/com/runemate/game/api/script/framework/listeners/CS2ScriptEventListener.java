package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface CS2ScriptEventListener extends EventListener {

    void onScriptExecutionStarted(CS2ScriptEvent event);

//    void onScriptExecutionFinished(CS2ScriptEvent event);

}
