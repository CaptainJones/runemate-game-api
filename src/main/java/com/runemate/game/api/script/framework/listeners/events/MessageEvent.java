package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;

public class MessageEvent implements Event {
    private final String sender, message;
    private final Chatbox.Message.Type type;

    public MessageEvent(int type, String sender, String message) {
        this.type = Chatbox.Message.Type.resolve(type, message, sender);
        this.sender = sender;
        this.message = message;
    }

    @Deprecated
    public String getSender() {
        return getSpeaker();
    }

    //In what world is this a better name than getSender()?
    public String getSpeaker() {
        return JagTags.remove(sender);
    }

    public String getMessage() {
        return JagTags.remove(message);
    }

    public boolean isPlayerModerator() {
        return sender.startsWith("<img=0>");
    }

    public boolean isJagexModerator() {
        return sender.startsWith("<img=1>");
    }

    public Chatbox.Message.Type getType() {
        return type;
    }

    @Override
    public String toString() {
        String display = "";
        if (sender != null) {
            display += '[' + sender + "] ";
        }
        display += '"' + message + '"';
        if (type != Chatbox.Message.Type.UNKNOWN) {
            display += " [" + type + ']';
        }
        return display;
    }
}
