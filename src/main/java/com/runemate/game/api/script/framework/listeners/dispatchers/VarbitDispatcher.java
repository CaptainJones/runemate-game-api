package com.runemate.game.api.script.framework.listeners.dispatchers;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public class VarbitDispatcher extends IngameEventDispatcher {
    private int[] varpSnapshot;
    private Map<Integer, ArrayList<Varbit>> varpToVarbits;

    @Override
    public int getIterationRateInMilliseconds() {
        return 300;
    }

    @Override
    public void dispatch(EventDispatcher dispatcher) {
        if (varpSnapshot == null) {
            varpSnapshot = Varps.snapshot();
            varpToVarbits = mapVarpsToVarbits();
        } else {
            int[] newVarpSnapshot = Varps.snapshot();
            for (int index = 0; index < varpSnapshot.length && index < newVarpSnapshot.length;
                 ++index) {
                int oldVal = varpSnapshot[index];
                int newVal = newVarpSnapshot[index];
                if (oldVal != newVal) {
                    dispatcher.dispatchLater(new VarpEvent(new Varp(index), oldVal, newVal));
                    if (varpToVarbits.containsKey(index)) {
                        for (Varbit varbit : varpToVarbits.get(index)) {
                            int oldVarbitValue = varbit.getValue(oldVal);
                            int newVarbitValue = varbit.getValue(newVal);
                            if (oldVarbitValue != newVarbitValue) {
                                dispatcher.dispatchLater(
                                    new VarbitEvent(varbit, oldVarbitValue, newVarbitValue));
                            }
                        }
                    }
                }
            }
            varpSnapshot = newVarpSnapshot;
        }
    }

    @Override
    public void clear() {
        varpSnapshot = null;
        varpToVarbits = null;
    }

    private Map<Integer, ArrayList<Varbit>> mapVarpsToVarbits() {
        Map<Integer, ArrayList<Varbit>> map = new HashMap<>();
        for (Varbit varbit : Varbits.loadAll()) {
            if (varbit.getVarDomain().equals(Varbit.Domain.PLAYER)) {
                ArrayList<Varbit> list = map.getOrDefault(varbit.getVarId(), new ArrayList<>());
                list.add(varbit);
                map.put(varbit.getVarId(), list);
            }
        }
        for (ArrayList<Varbit> varbits : map.values()) {
            varbits.trimToSize();
        }
        return map;
    }
}
