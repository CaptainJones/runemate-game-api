package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.*;

public class EngineStateEvent implements Event {

    private final RuneScape.EngineState previousEngineState, currentEngineState;

    public EngineStateEvent(int old, int current) {
        previousEngineState = RuneScape.EngineState.of(old);
        currentEngineState = RuneScape.EngineState.of(current);
    }

    public RuneScape.EngineState getPreviousState() {
        return previousEngineState != null ? previousEngineState : RuneScape.EngineState.UNKNOWN;
    }

    public RuneScape.EngineState getCurrentState() {
        return currentEngineState != null ? currentEngineState : RuneScape.EngineState.UNKNOWN;
    }

    @Override
    public String toString() {
        return "EngineStateEvent(previous=" + getPreviousState().name() + ", current="
            + getCurrentState().name() + ")";
    }
}
