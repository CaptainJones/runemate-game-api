package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.local.*;

public class SkillEvent implements Event {
    private final Skill skill;
    private final Type type;
    private final int current, previous;

    public SkillEvent(final Skill skill, Type type, int current, int previous) {
        this.skill = skill;
        this.type = type;
        this.current = current;
        this.previous = previous;
    }

    public Skill getSkill() {
        return skill;
    }

    public Type getType() {
        return type;
    }

    public int getCurrent() {
        return current;
    }

    public int getPrevious() {
        return previous;
    }

    public int getChange() {
        return Math.max(0, current - previous);
    }

    @Override
    public String toString() {
        int change = getChange();
        return skill + " gained " + getChange() + ' '
            + (type == Type.LEVEL_GAINED ? change == 1 ? "level" : "levels" : "experience");
    }

    public enum Type {
        LEVEL_GAINED,
        EXPERIENCE_GAINED
    }
}
