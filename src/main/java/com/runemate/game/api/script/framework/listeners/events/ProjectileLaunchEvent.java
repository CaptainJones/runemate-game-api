package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;

public class ProjectileLaunchEvent implements Event {
    private final Projectile projectile;

    public ProjectileLaunchEvent(Projectile projectile) {
        this.projectile = projectile;
    }

    public Projectile getProjectile() {
        return projectile;
    }

    @Override
    public String toString() {
        return "ProjectileLaunchEvent{projectile=" + projectile + "}";
    }
}
