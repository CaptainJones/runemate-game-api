package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.net.*;

public class GrandExchangeEvent implements Event {

    private final GrandExchange.Slot changedSlot;
    private final Type type;

    public GrandExchangeEvent(GrandExchange.Slot changedSlot, Type type) {
        this.changedSlot = changedSlot;
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public GrandExchange.Slot getSlot() {
        return changedSlot;
    }

    @Override
    public String toString() {
        return String.format("GrandExchangeEvent(slot=%s, type=%s)", changedSlot.getIndex(), type);
    }

    public enum Type {
        OFFER_CREATED,
        OFFER_REMOVED,
        OFFER_STATE_CHANGED,
        OFFER_COMPLETION_CHANGED
    }
}
