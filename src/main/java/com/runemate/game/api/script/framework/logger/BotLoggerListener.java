package com.runemate.game.api.script.framework.logger;

import java.util.*;

public interface BotLoggerListener extends EventListener {
    void logged(LoggedMessageEvent message);
}
