package com.runemate.game.api.rs3.queries;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.rs3.local.*;
import com.runemate.game.api.rs3.local.hud.eoc.*;
import com.runemate.game.api.rs3.queries.results.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

@Deprecated
public class ActionBarQueryBuilder
    extends QueryBuilder<ActionBar.Slot, ActionBarQueryBuilder, ActionBarQueryResults> {

    private static int getAbilityVarpIndex(int actionbar, int index) {
        return 0;
    }

    private static int getItemVarpIndex(int actionbar, int index) {
        return 0;
    }

    @Override
    public Callable<List<? extends ActionBar.Slot>> getDefaultProvider() {
        return Collections::emptyList;
    }

    @Override
    public ActionBarQueryBuilder get() {
        return this;
    }

    @Override
    protected ActionBarQueryResults results(
        Collection<? extends ActionBar.Slot> entries,
        ConcurrentMap<String, Object> cache
    ) {
        return new ActionBarQueryResults(entries, cache);
    }

    public final ActionBarQueryBuilder empty(final boolean empty) {
        return this;
    }

    public final ActionBarQueryBuilder filled(final boolean filled) {
        return this;
    }

    public final ActionBarQueryBuilder ready(final boolean ready) {
        return this;
    }

    public final ActionBarQueryBuilder selected(final boolean selected) {
        return this;
    }

    public final ActionBarQueryBuilder automaticallyActivatable(
        final boolean automaticallyActivatable
    ) {
        return this;
    }

    public final ActionBarQueryBuilder activatable(final boolean activatable) {
        return this;
    }

    public final ActionBarQueryBuilder coolingDown(final boolean coolingDown) {
        return this;
    }

    public final ActionBarQueryBuilder names(final String... names) {
        return this;
    }

    public final ActionBarQueryBuilder names(final Pattern... names) {
        return this;
    }

    public final ActionBarQueryBuilder type(ActionBar.Slot.ContentType type) {
        return this;
    }

    @Override
    public boolean accepts(ActionBar.Slot argument) {
        return false;
    }
}
