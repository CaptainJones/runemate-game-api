package com.runemate.game.api.rs3.local.hud.interfaces;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.rs3.entities.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.logger.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

@Deprecated
public final class BeastOfBurden {

    private BeastOfBurden() {
    }

    /**
     * Closes your beast of burdens inventory
     */
    public static boolean close() {
        return false;
    }

    public static InteractableRectangle getBoundsOf(final int index) {
        return null;
    }

    public static SpriteItem getItemIn(final int index) {
        return null;
    }

    /**
     * When the Beast Of Burden interface is open, returns a query containing all items your beast of burden contains.
     */
    public static SpriteItemQueryResults getItems() {
        return new SpriteItemQueryResults(Collections.emptyList());
    }

    /**
     * When the Beast Of Burden interface is open, returns a query containing all items your beast of burden contains that match the given filter.
     */
    public static SpriteItemQueryResults getItems(final Predicate<SpriteItem> filter) {
        return new SpriteItemQueryResults(Collections.emptyList());
    }

    public static List<InteractableRectangle> getSlotBounds() {
        return Collections.emptyList();
    }

    /**
     * Checks whether your beast of burdens inventory is open
     */
    public static boolean isOpen() {
        return false;
    }

    public static SpriteItemQueryBuilder newQuery() {
        return new SpriteItemQueryBuilder(Inventories.Documented.BEAST_OF_BURDEN);
    }

    /**
     * Opens your beast of burdens inventory
     */
    public static boolean open() {
        return false;
    }

    public static boolean store(String name, int amount) {
        return false;
    }

    /**
     * Stores (deposits) the specified amount of the first item that matches the filter.
     */
    public static boolean store(Predicate<SpriteItem> itemFilter, int amount) {
        return false;
    }

    /**
     * Stores (deposits) the specified amount of the specified item in the Beast of Burden
     */
    public static boolean store(SpriteItem item, int amount) {
        return false;
    }

    public static boolean withdraw(String name, int amount) {
        return false;
    }

    /**
     * Withdraws the specified amount of the first item that matches the filter from the beast of burden.
     */
    public static boolean withdraw(Predicate<SpriteItem> itemFilter, int amount) {
        return false;
    }

    /**
     * Withdraws the specified amount of a specific item from the beast of burden.
     */
    public static boolean withdraw(SpriteItem item, int amount) {
        return false;
    }

    /**
     * Checks if any items match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if an item matches the filter
     */
    public static boolean contains(Predicate<SpriteItem> filter) {
        return false;
    }

    /**
     * Checks if any items match the given id
     *
     * @param id the id to check the items against
     * @return true if an item matches the id
     */
    public static boolean contains(int id) {
        return false;
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(String name) {
        return false;
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(Pattern name) {
        return false;
    }

    /**
     * Checks if the supplied {@link Predicate filter} matches at least one item
     *
     * @param predicate the predicate to check the items against
     * @return true if the predicate matches an item
     */
    public static boolean containsAllOf(final Predicate<SpriteItem> predicate) {
        return false;
    }

    /**
     * Checks if all of the supplied {@link Predicate filter}s match at least one item each
     *
     * @param filters the predicates to check the items against
     * @return true if all of the predicates have a match
     */
    @SafeVarargs
    public static boolean containsAllOf(final Predicate<SpriteItem>... filters) {
        return false;
    }

    /**
     * Checks if all of the supplied ids match at least one item each
     *
     * @param ids the ids to check the items against
     * @return true if all of the ids have a match
     */
    public static boolean containsAllOf(final int... ids) {
        return false;
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final String... names) {
        return false;
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final Pattern... names) {
        return false;
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if at least one item doesn't match the filter
     */
    public static boolean containsAnyExcept(final Predicate<SpriteItem> filter) {
        return false;
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}s
     *
     * @param filters the predicates to check the items against
     * @return true if at least one item doesn't match the filters
     */
    @SafeVarargs
    public static boolean containsAnyExcept(final Predicate<SpriteItem>... filters) {
        return false;
    }

    /**
     * Checks if any items don't match the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item doesn't match the ids
     */
    public static boolean containsAnyExcept(final int... ids) {
        return false;
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final String... names) {
        return false;
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final Pattern... names) {
        return false;
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if at least one item matches the filter
     */
    public static boolean containsAnyOf(final Predicate<SpriteItem> filter) {
        return false;
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if at least one item matches a filter
     */
    @SafeVarargs
    public static boolean containsAnyOf(final Predicate<SpriteItem>... filters) {
        return false;
    }

    /**
     * Checks if any item matches the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item matches an id
     */
    public static boolean containsAnyOf(final int... ids) {
        return false;
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final String... names) {
        return false;
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final Pattern... names) {
        return false;
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if all items match the filter
     */
    public static boolean containsOnly(final Predicate<SpriteItem> filter) {
        return false;
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if all items match at least one filter each
     */
    @SafeVarargs
    public static boolean containsOnly(final Predicate<SpriteItem>... filters) {
        return false;
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final String... names) {
        return false;
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final Pattern... names) {
        return false;
    }

    /**
     * Gets the total quantity of items
     *
     * @return the total quantity of items
     */
    public static int getQuantity() {
        return 0;
    }

    /**
     * Gets the total quantity of items matching the filter
     *
     * @param filter the filter to check the items against
     * @return the total quantity of items matching the filter
     */
    public static int getQuantity(final Predicate<SpriteItem> filter) {
        return 0;
    }

    /**
     * Gets the total quantity of items matching the {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return the total quantity of items matching the filters
     */
    @SafeVarargs
    public static int getQuantity(final Predicate<SpriteItem>... filters) {
        return 0;
    }

    /**
     * Gets the total quantity of items matching the ids
     *
     * @param ids the ids to check the items against
     * @return the total quantity of items matching the ids
     */
    public static int getQuantity(final int... ids) {
        return 0;
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final String... names) {
        return 0;
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final Pattern... names) {
        return 0;
    }

}
