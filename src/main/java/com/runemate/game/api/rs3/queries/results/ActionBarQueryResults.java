package com.runemate.game.api.rs3.queries.results;

import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.rs3.local.hud.eoc.*;
import java.util.*;
import java.util.concurrent.*;

@Deprecated
public class ActionBarQueryResults extends QueryResults<ActionBar.Slot, ActionBarQueryResults> {
    public ActionBarQueryResults(final Collection<? extends ActionBar.Slot> results) {
        super(results);
    }

    public ActionBarQueryResults(
        final Collection<? extends ActionBar.Slot> results,
        ConcurrentMap<String, Object> cache
    ) {
        super(results, cache);
    }

    @Override
    protected ActionBarQueryResults get() {
        return this;
    }
}
