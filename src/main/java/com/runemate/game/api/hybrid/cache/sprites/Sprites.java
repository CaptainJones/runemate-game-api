package com.runemate.game.api.hybrid.cache.sprites;

import com.runemate.game.api.hybrid.cache.*;
import java.util.*;

public class Sprites {
    public static Sprite load(int id) {
        return deserialize(Js5Cache.load(8, id, 0));
    }

    public static List<Sprite> loadAll() {
        return null;
    }

    private static Sprite deserialize(byte[] serialized) {
        return null;
    }
}
