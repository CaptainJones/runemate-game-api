package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.util.*;
import java.io.*;
import java.util.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class QuestRequirement extends WebRequirement implements SerializableRequirement {
    private String name;
    private Quest.Status status;

    public QuestRequirement(String name, Quest.Status status) {
        this.name = name;
        this.status = status;
    }

    public QuestRequirement(int protocol, ObjectInput stream) {
        deserialize(protocol, stream);
    }

    @Override
    public boolean isMet0() {
        Quest quest = Quests.get(name);
        int status = quest != null ? quest.getStatus().ordinal() : Quest.Status.UNKNOWN.ordinal();
        if (status == Quest.Status.UNKNOWN.ordinal()) {
            return false;
        }
        if (this.status.ordinal() == status) {
            //The quest has the status that it needs to fulfill the requirement
            return true;
        }
        //The quest is required to at least be in progress and is completed, fulfilling the requirement.
        return this.status.ordinal() == 1 && status == 2;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(name);
        builder.append(status);
        return builder.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof QuestRequirement) {
            QuestRequirement that = (QuestRequirement) o;
            return Objects.equals(this.name, that.name) && Objects.equals(this.status, that.status);
        }
        return false;
    }

    @Override
    public int getOpcode() {
        return 7;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(name);
        stream.writeInt(status.ordinal());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        name = stream.readUTF();
        status = Enums.safeValueAt(Quest.Status.class, stream.readInt());
        return true;
    }
}