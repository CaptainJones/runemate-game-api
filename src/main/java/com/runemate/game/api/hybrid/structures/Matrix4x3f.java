package com.runemate.game.api.hybrid.structures;

public class Matrix4x3f {
    public float m11, m21, m31, m41,
        m12, m22, m32, m42,
        m13, m23, m33, m43;

    public Matrix4x3f() {
        identity();
    }

    public void identity() {
        m43 = 0.0F;
        m42 = 0.0F;
        m41 = 0.0F;
        m32 = 0.0F;
        m31 = 0.0F;
        m23 = 0.0F;
        m21 = 0.0F;
        m13 = 0.0F;
        m12 = 0.0F;
        m33 = 1.0F;
        m22 = 1.0F;
        m11 = 1.0F;
    }

    public boolean isIdentity() {
        return m11 == 1.0F && m22 == 1.0F && m33 == 1.0F
            && m12 == 0.0F && m13 == 0.0F
            && m21 == 0.0F && m23 == 0.0F
            && m31 == 0.0F && m32 == 0.0F
            && m41 == 0.0F && m42 == 0.0F && m43 == 0.0F;
    }

    public Matrix4x3f add(float m41, float m42, float m43) {
        this.m41 += m41;
        this.m42 += m42;
        this.m43 += m43;
        return this;
    }

    public void combine(
        final Vector3f one, final Vector3f two, float var13, float var14,
        float var15
    ) {
        float xDiff = two.x - one.x;
        float zDiff = two.z - one.z;
        float yDiff = two.y - one.y;
        float var19 = var14 * yDiff - var15 * zDiff;
        float var20 = var15 * xDiff - var13 * yDiff;
        float var21 = var13 * zDiff - var14 * xDiff;
        float var22 = (float) (1.0D / Math.sqrt(var19 * var19 + var20 * var20 + var21 * var21));
        float var23 = (float) (1.0D / Math.sqrt(xDiff * xDiff + zDiff * zDiff + yDiff * yDiff));
        this.m11 = var19 * var22;
        this.m21 = var20 * var22;
        this.m31 = var21 * var22;
        this.m13 = xDiff * var23;
        this.m23 = zDiff * var23;
        this.m33 = yDiff * var23;
        this.m12 = this.m23 * this.m31 - this.m33 * this.m21;
        this.m22 = this.m33 * this.m11 - this.m13 * this.m31;
        this.m32 = this.m13 * this.m21 - this.m23 * this.m11;
        this.m41 = -(
            (float) (
                one.x * (double) this.m11 + one.z * (double) this.m21 +
                    one.y * (double) this.m31
            )
        );
        this.m42 = -(
            (float) (
                one.x * (double) this.m12 + one.z * (double) this.m22 +
                    one.y * (double) this.m32
            )
        );
        this.m43 = -(
            (float) (
                one.x * (double) this.m13 + one.z * (double) this.m23 +
                    one.y * (double) this.m33
            )
        );
    }
}
