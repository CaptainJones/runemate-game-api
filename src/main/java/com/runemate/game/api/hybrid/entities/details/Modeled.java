package com.runemate.game.api.hybrid.entities.details;

import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.util.collections.*;
import javax.annotation.*;

public interface Modeled extends Renderable {
    /**
     * Gets the polygon-based model of the entity
     *
     * @return the entities model, otherwise the backup if set, if neither are available null
     */
    @Nullable
    Model getModel();

    /**
     * Sets a backup model to be used when the model isn't available.
     */
    void setBackupModel(int[] frontBottomLeft, int[] backTopRight);

    /**
     * Sets a pair of points to be used to create a backup model when the in-game model isn't available
     */
    void setBackupModel(Pair<int[], int[]> values);

    void setBackupModel(Model backup);

    /**
     * Sets a model to be used regardless of whether the in-game model is available.
     */
    void setForcedModel(int[] frontBottomLeft, int[] backTopRight);

    /**
     * Sets a pair of points to be used to create a model regardless of whether the in-game model is available
     */
    void setForcedModel(Pair<int[], int[]> values);

    void setForcedModel(Model forced);
}
