package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.utilities;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import javax.annotation.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class MusicianVertex extends UtilityVertex implements SerializableVertex {
    private Pattern name;

    public MusicianVertex(Coordinate position, String name) {
        this(position, Regex.getPatternForExactString(name));
    }

    public MusicianVertex(Coordinate position, Pattern name) {
        super(position, Collections.emptyList());
        this.name = name;
    }

    public MusicianVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition()).append(getName().pattern()).toHashCode();
    }

    @Override
    public int getOpcode() {
        return 13;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(name.pattern());
        stream.writeInt(name.flags());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.name = Pattern.compile(stream.readUTF(), stream.readInt());
        return true;
    }

    public Pattern getName() {
        return name;
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "MusicianVertex(name=" + getName() +
            ", x=" + position.getX() + ", y=" + position.getY() + ", plane=" + position.getPlane() +
            ')';
    }

    @Override
    public boolean step() {
        if (Traversal.isResting()) {
            return true;
        }
        Npc musician = getMusician();
        if (musician != null && musician.isVisible() &&
            musician.interact(Regex.getPatternForExactStrings("Listen to", "Listen-to"))) {
            Player local = Players.getLocal();
            return local != null &&
                Execution.delayUntil(Traversal::isResting, local::isMoving, 1500, 2500);
        }
        return false;
    }

    @Nonnull
    @Override
    public Pair<WebVertex, WebPath.VertexSearchAction> getStep(Map<String, Object> args) {
        final Npc musician = getMusician();
        return new Pair<>(
            musician != null && musician.isVisible() ? this : null,
            WebPath.VertexSearchAction.STOP
        );
    }

    public Npc getMusician() {
        final NpcQueryBuilder builder = Npcs.newQuery().on(getPosition());
        if (name != null) {
            builder.names(name);
        }
        return builder.results().first();
    }
}
