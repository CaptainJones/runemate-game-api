package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import java.io.*;
import lombok.*;

public class MembersRequirement extends WebRequirement implements SerializableRequirement {
    private boolean f2pAccessible, p2pAccessible;

    public MembersRequirement() {
        this(false, true);
    }

    public MembersRequirement(boolean f2pAccessible, boolean p2pAccessible) {
        this.f2pAccessible = f2pAccessible;
        this.p2pAccessible = p2pAccessible;
    }

    public MembersRequirement(int protocol, ObjectInput stream) {
        super(protocol, stream);
    }

    @Override
    public boolean isMet0() {
        if (Environment.isOSRS()) {
            boolean membersOnly = OSRSWorldSelect.isCurrentWorldMembersOnly();
            return membersOnly == p2pAccessible || !membersOnly == f2pAccessible;
        }
        WorldOverview wo = Worlds.getCurrentOverview();
        if (wo != null) {
            boolean membersOnly = wo.isMembersOnly();
            return membersOnly == p2pAccessible || !membersOnly == f2pAccessible;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof MembersRequirement
            && f2pAccessible == ((MembersRequirement) o).f2pAccessible &&
            p2pAccessible == ((MembersRequirement) o).p2pAccessible;
    }

    @Override
    public int hashCode() {
        int result = Boolean.hashCode(f2pAccessible);
        result = 31 * result + Boolean.hashCode(p2pAccessible);
        return result;
    }

    @Override
    public int getOpcode() {
        return 4;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeByte(
            f2pAccessible && p2pAccessible ? 3 : f2pAccessible ? 2 : p2pAccessible ? 1 : 0);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        int value = stream.readByte();
        if (value == 0) {
            f2pAccessible = false;
            p2pAccessible = false;
        } else if (value == 1) {
            f2pAccessible = false;
            p2pAccessible = true;
        } else if (value == 2) {
            f2pAccessible = true;
            p2pAccessible = false;
        } else if (value == 3) {
            f2pAccessible = true;
            p2pAccessible = true;
        } else {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MembersRequirement[F2P:" + f2pAccessible + " P2P:" + p2pAccessible + ']';
    }
}
