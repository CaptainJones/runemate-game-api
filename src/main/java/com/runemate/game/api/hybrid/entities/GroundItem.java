package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.entities.details.*;

/**
 * An item that has either fallen or been dropped on the ground
 */
public interface GroundItem extends Item, LocatableEntity, Modeled {
}
