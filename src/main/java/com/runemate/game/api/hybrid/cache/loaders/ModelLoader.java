package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class ModelLoader
    extends SerializedFileLoader<com.runemate.game.api.hybrid.cache.elements.CacheModel> {
    private final boolean rs3;

    public ModelLoader(boolean rs3) {
        super(rs3 ? Js5Cache.RS3Archives.JS5_MODELS : Js5Cache.OSRSArchives.JS5_MODELS);
        this.rs3 = rs3;
    }

    @Override
    protected com.runemate.game.api.hybrid.cache.elements.CacheModel construct(
        int entry, int file,
        Map<String, Object> arguments
    ) {
        return new CacheModel(rs3, entry);
    }
}
