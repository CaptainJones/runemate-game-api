package com.runemate.game.api.hybrid.entities.status;

/**
 * An icon displayed over the head of npcs and players due to combat status' and ailments.
 */
public final class OverheadIcon {
    private final int index;
    private final Type type;

    public OverheadIcon(final int index) {
        this(index, Type.UNKNOWN);
    }

    public OverheadIcon(final int index, final Type type) {
        this.index = index;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OverheadIcon that = (OverheadIcon) o;
        return index == that.index;
    }

    @Override
    public int hashCode() {
        return index;
    }

    @Override
    public String toString() {
        return "OverheadIcon(id=" + index + ", type=" + type.name() + ')';
    }

    /**
     * Gets the icons id
     */
    public int getId() {
        return index;
    }

    public Type getType() {
        return type;
    }

    public enum Type {
        SKULL,
        PRAYER,
        UNKNOWN
    }
}
