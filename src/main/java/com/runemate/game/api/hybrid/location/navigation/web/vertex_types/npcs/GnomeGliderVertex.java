package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.npcs;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import java.util.function.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class GnomeGliderVertex extends NpcVertex implements SerializableVertex {

    private String interfaceAction;

    public GnomeGliderVertex(
        Coordinate position, String interfaceAction,
        Collection<WebRequirement> requirements
    ) {
        super(position, requirements);
        this.interfaceAction = interfaceAction;
    }

    public GnomeGliderVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    @Override
    public Npc getNpc() {
        final NpcQueryBuilder builder = Npcs.newQuery().within(getWalkingBounds());
        builder.names(Regex.getPatternForContainsString("Captain "));
        builder.actions("Glider");
        if (provider != null) {
            builder.provider(() -> provider);
        }
        return builder.results().first();
    }

    @Override
    public int getOpcode() {
        return 20;
    }

    @Override
    @SneakyThrows(IOException.class)
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(interfaceAction);
        return true;
    }

    @Override
    @SneakyThrows(IOException.class)
    public boolean deserialize(int protocol, ObjectInput stream) {
        interfaceAction = stream.readUTF();
        return true;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition()).append(interfaceAction).toHashCode();
    }

    @Override
    public boolean step() {
        InterfaceComponent travelButton = getOSRSInterfaceComponent();
        if (travelButton == null) {
            Npc captain = getNpc();
            if (captain != null
                && (captain.isVisible() || Camera.turnTo(captain))
                && captain.interact("Glider", Regex.getPatternForContainsString("Captain "))) {
                Player avatar = Players.getLocal();
                if (Execution.delayUntil(() -> getOSRSInterfaceComponent() != null, avatar::isMoving, 1200, 2400)) {
                    travelButton = getOSRSInterfaceComponent();
                }
            }
        }
        if (travelButton != null && travelButton.interact(interfaceAction)) {
            return Execution.delayWhile(travelButton::isVisible, 3600, 4800);
        }
        return false;
    }

    @Override
    public String toString() {
        return "GnomeGliderVertex(destination=" + interfaceAction + ')';
    }

    @Override
    public Predicate<Npc> getFilter() {
        final NpcQueryBuilder builder = Npcs.newQuery().within(getWalkingBounds());
        builder.names(Regex.getPatternForContainsString("Captain "));
        builder.actions("Glider");
        if (provider != null) {
            builder.provider(() -> provider);
        }
        return builder::accepts;
    }

    @Override
    public void invalidateCache() {
        provider = null;
    }

    private InterfaceComponent getOSRSInterfaceComponent() {
        return Interfaces.newQuery().containers(138).types(InterfaceComponent.Type.MODEL)
            .actions(interfaceAction).visible().results().first();
    }

}
