package com.runemate.game.api.hybrid.entities.details;

import javax.annotation.*;

/**
 * Anything that has a name, the opposite of anonymous.
 */
@FunctionalInterface
public interface Onymous {
    @Nullable
    String getName();
}
