package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import com.runemate.game.api.hybrid.local.*;
import java.io.*;
import lombok.*;

public class SkillRequirement extends WebRequirement implements SerializableRequirement {
    private int level;
    private Skill skill;

    public SkillRequirement(Skill skill, int level) {
        this.skill = skill;
        this.level = level;
    }

    public SkillRequirement(int protocol, ObjectInput stream) {
        super(protocol, stream);
    }

    public int getLevel() {
        return level;
    }

    @Override
    public int getOpcode() {
        return 0;
    }

    @Override
    @SneakyThrows(IOException.class)
    public boolean serialize(ObjectOutput stream) {
        stream.writeByte(skill.getIndex());
        stream.writeShort(level);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        int skillIndex = stream.readByte();
        skill = Skills.getByIndex(skillIndex);
        if (skill == null) {
            throw new IllegalArgumentException("Unknown skill index: " + skillIndex);
        }
        level = stream.readShort();
        return true;
    }

    public Skill getSkill() {
        return skill;
    }

    @Override
    public boolean isMet0() {
        return skill.getCurrentLevel() >= this.level;
    }

    @Override
    public int hashCode() {
        int result = skill.hashCode();
        result = 31 * result + level;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof SkillRequirement) {
            SkillRequirement that = (SkillRequirement) o;
            return level == that.level && skill == that.skill;
        }
        return false;
    }

    @Override
    public String toString() {
        return "SkillRequirement(" + skill + ' ' + level + ')';
    }

}
