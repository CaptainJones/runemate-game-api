package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class MaterialLoader
    extends SerializedFileLoader<com.runemate.game.api.hybrid.cache.elements.CacheMaterial> {
    private final boolean rs3;

    public MaterialLoader(boolean rs3) {
        super(9);
        this.rs3 = rs3;
    }

    @Override
    protected CacheMaterial construct(int entry, int file, Map<String, Object> arguments) {
        return new OSRSCacheMaterial(file);
    }
}
