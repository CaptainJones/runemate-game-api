package com.runemate.game.api.hybrid.cache.materials;

import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.internal.exception.*;
import com.runemate.game.cache.*;
import java.io.*;
import java.util.*;

public class Materials {
    private static final MaterialLoader RS3_LOADER = new MaterialLoader(true);
    private static final MaterialLoader OSRS_LOADER = new MaterialLoader(false);

    public static Material load(int id) {
        try {
            return (OSRS_LOADER).load(JS5CacheController.getLargestJS5CacheController(), 0, id);
        } catch (IOException e) {
            throw new UnableToParseBufferException(
                "Failed to load the material with id " + id + " from the Js5Cache.", e);
        }
    }

    public static List<? extends Material> loadAll() {
        return (OSRS_LOADER).load(JS5CacheController.getLargestJS5CacheController(), 0);
    }
}
