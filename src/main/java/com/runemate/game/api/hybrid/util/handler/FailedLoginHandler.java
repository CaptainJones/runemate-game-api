package com.runemate.game.api.hybrid.util.handler;

import com.runemate.game.api.hybrid.*;

public abstract class FailedLoginHandler {

    public void handle(GameEvents.LoginManager.Fail fail) {
        handle(fail, 0);
    }

    public abstract void handle(GameEvents.LoginManager.Fail fail, int retriesThusFar);
}
