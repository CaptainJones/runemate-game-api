package com.runemate.game.api.hybrid.util;

import java.util.*;
import java.util.function.*;

@FunctionalInterface
public interface PentFunction<A, B, C, D, E, R> {
    R apply(A a, B b, C c, D d, E e);

    default <V> PentFunction<A, B, C, D, E, V> andThen(
        Function<? super R, ? extends V> after
    ) {
        Objects.requireNonNull(after);
        return (A a, B b, C c, D d, E e) -> after.apply(apply(a, b, c, d, e));
    }
}
