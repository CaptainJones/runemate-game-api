package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class NpcDefinitionLoader
    extends SerializedFileLoader<com.runemate.game.api.hybrid.cache.elements.CacheNpcDefinition> {
    private final boolean rs3;

    public NpcDefinitionLoader(boolean rs3, int file) {
        super(file);
        this.rs3 = rs3;
    }

    @Override
    protected com.runemate.game.api.hybrid.cache.elements.CacheNpcDefinition construct(
        int entry,
        int file,
        Map<String, Object> arguments
    ) {
        return new CacheNpcDefinition(rs3, rs3 ? (entry << 7) + file : file);
    }
}
