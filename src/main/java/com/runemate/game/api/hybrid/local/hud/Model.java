package com.runemate.game.api.hybrid.local.hud;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.structures.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.hybrid.util.shapes.*;
import java.awt.*;
import java.awt.geom.Area;
import java.util.List;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import javafx.scene.canvas.*;
import javax.annotation.*;

/**
 * A 3D in-game entity model
 */
public abstract class Model implements LocatableEntity, Rotatable, Animable {
    public static final int DEFAULT_HEIGHT_OFFSET = 0;
    protected final int floorHeight;
    protected final LocatableEntity owner;

    public Model(final LocatableEntity owner) {
        this(owner, DEFAULT_HEIGHT_OFFSET);
    }

    public Model(final LocatableEntity owner, final int floorHeight) {
        this.owner = owner;
        this.floorHeight = floorHeight;
    }

    @Nullable
    @Deprecated
    private static InteractableRectangle getBoundingRectangle(
        final Collection<Triangle> triangles
    ) {
        if (!triangles.isEmpty()) {
            int minX = Integer.MAX_VALUE;
            int minY = Integer.MAX_VALUE;
            int maxX = Integer.MIN_VALUE;
            int maxY = Integer.MIN_VALUE;
            for (final Triangle triangle : triangles) {
                Point a = triangle.getA();
                if (a.x > maxX) {
                    maxX = a.x;
                }
                if (a.x < minX) {
                    minX = a.x;
                }
                if (a.y > maxY) {
                    maxY = a.y;
                }
                if (a.y < minY) {
                    minY = a.y;
                }
                Point b = triangle.getB();
                if (b.x > maxX) {
                    maxX = b.x;
                }
                if (b.x < minX) {
                    minX = b.x;
                }
                if (b.y > maxY) {
                    maxY = b.y;
                }
                if (b.y < minY) {
                    minY = b.y;
                }
                Point c = triangle.getC();
                if (c.x > maxX) {
                    maxX = c.x;
                }
                if (c.x < minX) {
                    minX = c.x;
                }
                if (c.y > maxY) {
                    maxY = c.y;
                }
                if (c.y < minY) {
                    minY = c.y;
                }
            }
            int width = maxX - minX;
            int height = maxY - minY;
            if (width > 0 && height > 0) {
                return new InteractableRectangle(minX, minY, width, height);
            }
        }
        return null;
    }

    public LocatableEntity getOwner() {
        return owner;
    }

    @Nullable
    private InteractablePoint getInteractionPoint(@Nonnull List<Triangle> triangles) {
        if (triangles.isEmpty()) {
            if (Environment.isVerbose()) {
                System.err.println(
                    "[Verbose] No triangles were provided for point selection on " + owner);
                //System.err.println();
            }
            return null;
        }
        Triangle selected = null;
        triangles.sort(Comparator.comparingDouble(Triangle::getArea));
        if (triangles.get(triangles.size() - 1).getArea() == 0D) {
            //Remove the sides that have an invalid length
            triangles = Parallelize.collectToList(triangles, t -> t.getLongestSidesLength() > 0);
            //If all lines are of a length 0 we can't do anything
            if (triangles.isEmpty()) {
                if (Environment.isVerbose()) {
                    System.err.println(
                        "[Verbose] No lines are available for point selection after filtering by longest side length on " +
                            owner);
                    //System.err.println();
                }
                return null;
            }
            //Each triangle is a line, sort by length of the longest line.
            if (triangles.size() == 1) {
                selected = triangles.get(0);
                Line selectedSide = selected.getLongestSide();
                if (Environment.isVerbose()) {
                    //System.out.println("[Verbose] --Using the only available line for " + entity + " with length " + selected.getLongestSidesLength());
                }
                List<InteractablePoint> sidePoints = selectedSide.getPoints();
                //TODO Confirm that this will never return the exact value sidePoints.size()
                int index = (int) Random.nextGaussian(0, sidePoints.size());
                if (index == sidePoints.size()) {
                    System.out.println("[Alert] The index can indeed equal sidePoints.size()");
                }
                Point sidePoint = sidePoints.get((int) Random.nextGaussian(0, sidePoints.size()));
                if (Environment.isVerbose()) {
                    //System.out.println("[Verbose] --The selected line contains " + sidePoints.size() + " points, we picked " + sidePoint.toString().replace("java.awt.", "") + ".");
                    //System.out.println();
                }
                return new InteractablePoint(sidePoint);
            } else {
                triangles.sort(Comparator.comparingDouble(Triangle::getLongestSidesLength));
                //Pick a line based on it's length and a bellcurve.
                if (Environment.isVerbose()) {
                    //System.out.println("[Verbose] --The shortest line's length is " + triangles.get(0).getLongestSidesLength() + " and the largest's length is " + triangles.get(triangles.size() - 1).getLongestSidesLength() + ".");
                }
                double desiredLength = Random.nextGaussian(
                    triangles.get(0).getLongestSidesLength(),
                    triangles.get(triangles.size() - 1).getLongestSidesLength()
                );
                PairList<Triangle, Double> closest = new PairList<>(8);
                double closestLength = Double.POSITIVE_INFINITY;
                for (Triangle triangle : triangles) {
                    double difference = Math.abs(desiredLength - triangle.getLongestSidesLength());
                    if (difference <= closestLength) {
                        closest.removeIf(td -> td.getRight() > difference);
                        closest.add(triangle, difference);
                        closestLength = difference;
                    }
                }
                //If multiple lines have the same distance to the desired length, this ensures it doesn't pick the same line each time.
                Collections.shuffle(closest, Random.getRandom());
                selected = closest.get(0).getLeft();
                if (selected != null) {
                    //Pick a point along the line of the triangle with the longest side length
                    Line selectedSide = selected.getLongestSide();
                    if (Environment.isVerbose()) {
                        //System.out.println("[Verbose] --We used a bellcurve to determine a desired length of " + desiredLength + ".");
                        //System.out.println("[Verbose] --The closest we could get was " + closestLength + " so we picked a line which has a length of " + selected.getLongestSidesLength());
                    }
                    List<InteractablePoint> sidePoints = selectedSide.getPoints();
                    //TODO Confirm that this will never return the exact value sidePoints.size()
                    Point sidePoint =
                        sidePoints.get((int) Random.nextGaussian(0, sidePoints.size()));
                    if (Environment.isVerbose()) {
                        //System.out.println("[Verbose] --The selected line contains " + sidePoints.size() + " points, we picked " + sidePoint.toString().replace("java.awt.", "") + ".");
                        //System.out.println();
                    }
                    return new InteractablePoint(sidePoint);
                }
                return null;
            }
        }
        triangles = triangles.stream().filter(triangle -> triangle.getArea() > 0)
            .collect(Collectors.toList());
        if (triangles.isEmpty()) {
            if (Environment.isVerbose()) {
                //System.err.println("[Verbose] No triangles are available for point selection after filtering by area on " + entity);
                //System.err.println();
            }
            return null;
        }
        //If we have a single triangle, pick a point that's uniformly distributed within it, unless no integer points fall within it, then allow the selection of an edge point.
        if (triangles.size() == 1) {
            selected = triangles.get(0);
            if (Environment.isVerbose()) {
                //System.out.println("[Verbose] Only one triangle is available for " + entity + ", using our only choice " + selected);
            }
        } else {
            //Pick a triangle by using a bellcurve where the min is the smallest area and max is the largest area, then based on the value chosen find the triangle with the closest area to it.
            if (Environment.isVerbose()) {
                //System.out.println("[Verbose] Minimum area for " + entity + " is " + triangles.get(0).getArea() + " and maximum area is " + triangles.get(triangles.size() - 1).getArea());
            }
            double desiredArea = Random.nextGaussian(
                triangles.get(0).getArea(),
                triangles.get(triangles.size() - 1).getArea()
            );
            PairList<Triangle, Double> closest = new PairList<>(8);
            double closestArea = Double.POSITIVE_INFINITY;
            for (Triangle triangle : triangles) {
                double difference = Math.abs(desiredArea - triangle.getArea());
                if (difference <= closestArea) {
                    closest.removeIf(td -> td.getRight() > difference);
                    closest.add(triangle, difference);
                    closestArea = difference;
                }
            }
            //If multiple triangles have the same distance to the desired area, this ensures it doesn't pick the same line each time.
            Collections.shuffle(closest, Random.getRandom());
            selected = closest.get(0).getLeft();
            if (selected != null && Environment.isVerbose()) {
                //System.out.println("[Verbose] --" + triangles.size() + " triangles, used a bellcurve to pick a desired area of " + desiredArea + ".");
                //System.out.println("[Verbose] --Closest we could get to that was " + closestArea + " so we picked a triangle which has an area of " + selected.getArea());
            }
        }
        if (selected != null) {
            long ms = System.currentTimeMillis();
            Set<InteractablePoint> unique_points = selected.getPoints();
            if (Environment.isVerbose()) {
                ms = System.currentTimeMillis() - ms;
                //System.out.println("[Verbose] --Triangle#getPoints() took " + ms + "ms");
            }
            if (unique_points.isEmpty()) {
                if (Environment.isVerbose()) {
                    //System.out.println("[Verbose] --Triangle#getPoints() is empty, adding the points along the triangle's sides.");
                }
                for (Line side : selected.getSides()) {
                    unique_points.addAll(side.getPoints());
                }
                if (Environment.isVerbose()) {
                    //System.out.println("[Verbose] --Added " + unique_points.size() + " points from along the triangle's sides.");
                }
                if (unique_points.isEmpty()) {
                    if (Environment.isVerbose()) {
                        //System.out.println("[Verbose] --Unable to select a point from the triangle.");
                        //System.out.println();
                    }
                    return null;
                }
            }
            List<InteractablePoint> points = new ArrayList<>(unique_points);
            if (Environment.isVerbose()) {
                //System.out.println("[Verbose] --The selected triangle has " + points.size() + " points.");
            }
            InteractablePoint centroid = new InteractablePoint(
                (selected.getA().x + selected.getB().x + selected.getC().x) / 3,
                (selected.getA().y + selected.getB().y + selected.getC().y) / 3
            );
            if (Environment.isVerbose() && !selected.contains(centroid)) {
                //System.out.println("[Verbose] --The triangle's centroid isn't contained within the triangle's bounds, point selection will be biased towards an outer boundary.");
            }
            if (Environment.isVerbose()) {
                //System.out.println("[Verbose] --Sorting the triangle's points by their distance from its centroid " + centroid);
            }
            points.sort(Comparator.comparingDouble(
                o -> Distance.Algorithm.MANHATTAN.calculate(o.x, o.y, centroid.x, centroid.y)));
            int pointIndex = (int) Random.nextGaussian(0, points.size());
            if (Environment.isVerbose()) {
                //System.out.println("[Verbose] --Used normalized distribution on the sorted point list to decide to use the point at index " + pointIndex);
            }
            InteractablePoint destination = points.get(pointIndex);
            if (destination == null) {
                if (Environment.isVerbose()) {
                    //System.out.println("[Verbose] --Unable to select a normally distributed point from the triangle.");
                    //System.out.println();
                }
                return null;
            }
            if (Environment.isVerbose()) {
                //System.out.println("[Verbose] --Selected " + destination.toString().replace("java.awt.", "") + " from the triangle as the target point.");
                //System.out.println();
            }
            return destination;
        }
        return null;
    }

    public void animate(int animationId, int animationFrame, int stanceId, int stanceFrame) {
        //Not all model types can be animated such as bounding models. The animation should occur before the
        //bounding model is built.
    }

    public abstract List<Triangle> projectTriangles();

    public abstract List<Triangle> projectTrianglesWithin(Shape viewport);

    protected abstract int getTriangleCount();

    public abstract int getHeight();

    @Nullable
    public Polygon projectConvexHull() {
        return projectConvexHull(projectTriangles());
    }

    @Nullable
    private Polygon projectConvexHull(List<Triangle> projectedFaces) {
        return ConvexHulls.jarvisMarchOnFaces(projectedFaces);
    }

    @Override
    public void render(Graphics2D g2d) {
        Polygon convexHull = projectConvexHull();
        if (convexHull != null) {
            g2d.drawPolygon(convexHull);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        Polygon convexHull = projectConvexHull();
        if (convexHull != null) {
            gc.strokePolygon(Renderable.convert(convexHull.xpoints),
                Renderable.convert(convexHull.ypoints), convexHull.npoints
            );
        }
    }

    @Override
    public boolean isVisible() {
        return !projectTriangles().isEmpty();
    }

    @Override
    public final double getVisibility() {
        int mpc = getTriangleCount();
        if (mpc == 0) {
            return 0;
        }
        return (projectTriangles().size() * 100.0d) / mpc;
    }

    @Override
    public final InteractablePoint getInteractionPoint(Point origin) {
        return getInteractionPoint(projectTriangles());
    }

    @Override
    public final boolean contains(final Point point) {
        return contains(projectTriangles(), point);
    }

    private boolean contains(List<Triangle> triangles, final Point point) {
        Polygon hull = projectConvexHull(triangles);
        if (hull == null || !hull.contains(point)) {
            return false;
        }
        Area bounds = new Area();
        for (final Triangle triangle : triangles) {
            Polygon polygon = triangle.toPolygon();
            //TODO is it faster and/or more accurate to use polygon.contains(Point) or our Triangle.contains(Point)
            //if (triangle.contains(point)){
            if (polygon.contains(point)) {
                return true;
            }
            bounds.add(new Area(polygon));
        }
        return bounds.contains(point);
    }

    @Override
    public final boolean click() {
        return Mouse.click(owner, Mouse.Button.LEFT);
    }

    @Override
    public final boolean interact(final Pattern action, final Pattern target) {
        //remove maybe
        return Menu.click(owner, action, target);
    }

    @Nullable
    @Override
    public final Coordinate getPosition(Coordinate regionBase) {
        return owner != null ? owner.getPosition(regionBase) : null;
    }

    @Nullable
    @Override
    public final Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase) {
        return owner != null ? owner.getHighPrecisionPosition(regionBase) : null;
    }

    @Nullable
    @Override
    public com.runemate.game.api.hybrid.location.Area.Rectangular getArea(Coordinate regionBase) {
        return owner != null ? owner.getArea(regionBase) : null;
    }

    @Override
    public final int getHighPrecisionOrientation() {
        final int orientation;
        return owner instanceof Rotatable &&
            (orientation = ((Rotatable) owner).getHighPrecisionOrientation()) != -1 ? orientation :
            -1;
    }

    @Override
    public int getOrientationAsAngle() {
        return owner instanceof Rotatable ? ((Rotatable) owner).getOrientationAsAngle() : -1;
    }

    @Override
    public boolean isFacing(Locatable locatable) {
        return RotatableCommons.isFacing(this, locatable);
    }

    @Override
    public final int getAnimationId() {
        return owner instanceof Animable ? ((Animable) owner).getAnimationId() : -1;
    }

    /**
     * Gets the BoundingModel of this model.
     */
    @Nullable
    public abstract BoundingModel getBoundingModel();

    /**
     * Gets a bounding rectangle of the model.
     */
    @Deprecated
    public final InteractableRectangle projectBoundingRectangle() {
        return getBoundingRectangle(getBoundingModel().projectTriangles());
    }

    @Override
    public String toString() {
        return "Model(origin: " + owner + ")";
    }

    /**
     * Gets a list of colors that are used by default on the models polygons.
     * These are sometimes replaced before being rendered and these substitutions can be retrieved from
     * GameObjectDefinition#getColorSubstitutions and NpcDefinition#getColorSubstitutions.
     */
    public abstract Set<Color> getDefaultColors();

    @Override
    public Model getModel() {
        return this;
    }

    @Override
    public void setBackupModel(int[] frontBottomLeft, int[] backTopRight) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setBackupModel(Pair<int[], int[]> values) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setBackupModel(Model backup) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setForcedModel(int[] frontBottomLeft, int[] backTopRight) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setForcedModel(Pair<int[], int[]> values) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setForcedModel(Model forced) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Model model = (Model) o;
        return floorHeight == model.floorHeight && Objects.equals(owner, model.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(floorHeight, owner.hashCode());
    }
}
