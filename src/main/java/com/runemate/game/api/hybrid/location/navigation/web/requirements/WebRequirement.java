package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import java.io.*;

public abstract class WebRequirement {
    protected boolean inverted = false;
    private WebRequirement alternative;

    public WebRequirement() {
    }

    public WebRequirement(int protocol, ObjectInput stream) {
        if (this instanceof SerializableRequirement) {
            ((SerializableRequirement) this).deserialize(protocol, stream);
        }
    }

    @Deprecated
    public WebRequirement getAlternative() {
        return alternative;
    }

    /**
     * Adds an alternative requirement that can be met instead, returning this same object for chaining.
     */
    public WebRequirement or(WebRequirement requirement) {
        if (requirement == null) {
            throw new IllegalArgumentException(
                "The alternate requirement of " + this + " cannot be null");
        }
        if (alternative == null) {
            alternative = requirement;
            return this;
        }
        throw new IllegalStateException("Cannot have more than one alternate.");
    }

    @Deprecated
    public final boolean isAlternativeMet() {
        return alternative != null && (alternative.isMet() || alternative.isAlternativeMet());
    }

    protected abstract boolean isMet0();

    public final boolean isMet() {
        return inverted() != isMet0();
    }

    public WebRequirement invert() {
        this.inverted = !this.inverted;
        return this;
    }

    public boolean inverted() {
        return inverted;
    }

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object o);

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
