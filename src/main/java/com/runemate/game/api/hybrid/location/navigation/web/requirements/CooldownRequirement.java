package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import com.runemate.client.game.account.open.*;
import com.runemate.client.game.open.*;
import java.io.*;
import lombok.*;

public class CooldownRequirement extends WebRequirement implements SerializableRequirement {
    public String name;
    public Long cooldown;

    public CooldownRequirement(int protocol, ObjectInput stream) {
        super(protocol, stream);
    }

    public CooldownRequirement(String name, long cooldown) {
        this.name = name;
        this.cooldown = cooldown;
    }

    @Override
    protected boolean isMet0() {
        CachedTimer cachedTimer = (CachedTimer) OpenAccountDetails.getTraversalProfile().getCachedTimers().get(name);
        return cachedTimer == null || System.currentTimeMillis() - cachedTimer.getLastOccurrence() > cooldown;
    }

    @Override
    public int getOpcode() {
        return 10;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(name);
        stream.writeLong(cooldown);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.name = stream.readUTF();
        this.cooldown = stream.readLong();
        return true;
    }

    @Override
    public int hashCode() {
        long result = name.hashCode();
        result = 31 * result + cooldown;
        return (int) result;
    }

    @Override
    public boolean equals(Object o) {
        return this == o || o instanceof CooldownRequirement
            && ((CooldownRequirement) o).name.equals(this.name)
            && ((CooldownRequirement) o).cooldown == this.cooldown.longValue();
    }

    @Override
    public String toString() {
        return "CooldownRequirement(" + name + " every " + cooldown + "ms)";
    }
}
