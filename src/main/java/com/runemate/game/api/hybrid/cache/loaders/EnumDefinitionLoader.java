package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class EnumDefinitionLoader
    extends SerializedFileLoader<com.runemate.game.api.hybrid.cache.elements.CacheEnumDefinition> {

    public EnumDefinitionLoader(int archive, boolean rs3) {
        super(archive);
    }

    @Override
    protected com.runemate.game.api.hybrid.cache.elements.CacheEnumDefinition construct(
        int entry,
        int file,
        Map<String, Object> arguments
    ) {
        return new CacheEnumDefinition(file);
    }
}
