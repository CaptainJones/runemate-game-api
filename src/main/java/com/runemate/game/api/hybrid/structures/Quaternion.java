package com.runemate.game.api.hybrid.structures;

import com.runemate.game.cache.io.*;
import java.io.*;

public final class Quaternion implements Cloneable {
    private float i, j, k, a;

    public Quaternion() {
        this.k = 0.0f;
        this.j = 0.0f;
        this.i = 0.0f;
        this.a = 1.0f;
    }

    Quaternion(Js5InputStream packet) throws IOException {
        this.i = packet.readFloat();
        this.j = packet.readFloat();
        this.k = packet.readFloat();
        this.a = packet.readFloat();
    }

    public Quaternion(final float i, final float j, final float k, final float a) {
        this.i = i;
        this.j = j;
        this.k = k;
        this.a = a;
    }

    public float getI() {
        return i;
    }

    public float getJ() {
        return j;
    }

    public float getK() {
        return k;
    }

    public float getA() {
        return a;
    }

    public Quaternion invert() {
        this.i = -this.i;
        this.j = -this.j;
        this.k = -this.k;
        return this;
    }

    public Quaternion multiply(Quaternion quaternion) {
        float i2 = quaternion.a * this.i + quaternion.i * this.a + quaternion.j * this.k -
            quaternion.k * this.j;
        float j2 = quaternion.a * this.j - quaternion.i * this.k + quaternion.j * this.a +
            quaternion.k * this.i;
        float k2 = quaternion.a * this.k + quaternion.i * this.j - quaternion.j * this.i +
            quaternion.k * this.a;
        float a2 = quaternion.a * this.a - quaternion.i * this.i - quaternion.j * this.j -
            quaternion.k * this.k;
        this.i = i2;
        this.j = j2;
        this.k = k2;
        this.a = a2;
        return this;
    }

    @Override
    public Quaternion clone() {
        return new Quaternion(i, j, k, a);
    }
}
