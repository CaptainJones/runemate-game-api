package com.runemate.game.api.hybrid.entities.status;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.util.*;

public final class Hitsplat implements Validatable {

    private final int id, damage;
    private final int endCycle;
    private final int secondaryId;
    private final int secondaryDamage;

    public Hitsplat(final int id, final int damage, int secondaryId, int secondaryDamage, int endCycle) {
        this.id = id;
        this.damage = damage;
        this.secondaryId = secondaryId;
        this.secondaryDamage = secondaryDamage;
        this.endCycle = endCycle;
    }

    public int geId() {
        return id;
    }

    public int getSecondaryId() {
        return secondaryId;
    }

    public int getDamage() {
        return damage;
    }

    public int getSecondaryDamage() {
        return secondaryDamage;
    }

    @Deprecated
    public int getStartCycle() {
        return secondaryDamage;
    }

    /**
     * Gets the last cycle that the hitsplat is to be valid on.
     *
     * @return an int representing a game cycle.
     */
    public int getEndCycle() {
        return endCycle;
    }

    public Classification getClassification() {
        boolean osrs = Environment.isOSRS();
        for (Classification classification : Classification.values()) {
            for (int classifier : (
                osrs ? classification.osrs_classifiers : classification.rs3_classifiers
            )) {
                if (classifier == id) {
                    return classification;
                }
            }
        }
        return Classification.UNCLASSIFIED;
    }

    @Override
    public int hashCode() {
        return 31 * id + damage;
    }

    @Override
    public boolean equals(Object o) {
        if (this != o) {
            if (o instanceof Hitsplat) {
                Hitsplat hitsplat = (Hitsplat) o;
                return id == hitsplat.id && damage == hitsplat.damage && endCycle == hitsplat.endCycle;
            }
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Hitsplat{id=" + id + ", secondaryId=" + secondaryId + ", damage=" + damage + ", secondaryDamage=" + secondaryDamage
            + ", endCycle=" + endCycle + '}';
    }

    @Override
    public boolean isValid() {
        return endCycle > RuneScape.getCurrentCycle();
    }

    public enum Classification {
        UNCLASSIFIED(),
        MISS(0, new int[] { 0, 141 }),
        POISON(2, 142),
        //OSRS exclusives
        COMBAT(1, -1),
        VENOM(5, -1),
        DISEASE(-1, -1),
        //RS3 exclusives
        /**
         * Exclusive to damaging the bulwark beast
         */
        //ARMOUR_PENETRATING(-1, -2),
        MELEE_AUTO_ATTACK(-1, 132),
        MELEE_ABILITY(-1, 133),
        MELEE_CRITICAL_HIT(-1, 134),
        RANGED_AUTO_ATTACK(-1, 135),
        RANGED_ABILITY(-1, 136),
        RANGED_CRITICAL_HIT(-1, 137),
        MAGIC_AUTO_ATTACK(-1, 138),
        MAGIC_ABILITY(-1, 139),
        MAGIC_CRITICAL_HIT(-1, 140),
        HEALING(-1, 143),
        /**
         * Generic damage caused by desert heat, freezing cold, failing agility obstacles, many boss special attacks.
         */
        TYPELESS(-1, 144),
        //DEFLECTION(-1, -2),
        //CANNON(-1, -2),
        /**
         * Received by Solak and dealt by the Eldritch crossbow
         */
        //BLIGHT(-1, 232),
        //INSTANT_KILL(-1, -2),
        /**
         * When at least 32,768 life points are healed, which is possible on bosses with at least 655,360 maximum life points.
         */
        //UBER_HEALING(-1, -2)
        BIG_GAME_HUNTER_BLUE(233),
        BIG_GAME_HUNTER_YELLOW(235),
        BIG_GAME_HUNTER_RED(250);
        private final int[] osrs_classifiers;
        private final int[] rs3_classifiers;

        Classification() {
            this(-1);
        }

        Classification(int classifier) {
            this(classifier, classifier);
        }

        Classification(int osrs_classifier, int rs3_classifier) {
            this(new int[] { osrs_classifier }, new int[] { rs3_classifier });
        }

        Classification(int[] classifiers) {
            this(classifiers, classifiers);
        }

        Classification(int[] osrs_classifiers, int rs3_classifier) {
            this(osrs_classifiers, new int[] { rs3_classifier });
        }

        Classification(int osrs_classifier, int[] rs3_classifiers) {
            this(new int[] { osrs_classifier }, rs3_classifiers);
        }

        Classification(int[] osrs_classifiers, int[] rs3_classifiers) {
            this.osrs_classifiers = osrs_classifiers;
            this.rs3_classifiers = rs3_classifiers;
        }
    }
}
