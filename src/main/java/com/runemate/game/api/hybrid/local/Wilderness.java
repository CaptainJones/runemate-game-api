package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import java.util.regex.*;

public class Wilderness {
    private static final Pattern DEPTH_TEXT = Pattern.compile("^Level: (?<depth>\\d+)");

    public static short getDepth() {
        InterfaceComponent component = Interfaces.newQuery()
            .grandchildren(false).containers(Environment.isOSRS() ? 90 : 381)
            .types(InterfaceComponent.Type.LABEL)
            .texts(DEPTH_TEXT).visible().results().first();
        String text;
        if (component != null && (text = component.getText()) != null) {
            Matcher matcher = DEPTH_TEXT.matcher(text);
            return matcher.matches() ? Short.parseShort(matcher.group("depth")) : 0;
        }
        return 0;
    }
}
