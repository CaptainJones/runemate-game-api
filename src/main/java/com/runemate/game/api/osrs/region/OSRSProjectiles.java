package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.entities.*;
import java.util.*;
import java.util.function.*;

public final class OSRSProjectiles {
    private OSRSProjectiles() {
    }


    public static LocatableEntityQueryResults<Projectile> getLoaded(
        final Predicate<? super Projectile> filter
    ) {
        List<Long> projectileUids = OpenProjectile.getLoaded();
        List<Projectile> projectiles = new ArrayList<>(projectileUids.size());
        for (final long projectile_uid : projectileUids) {
            final Projectile projectile = new OSRSProjectile(projectile_uid);
            if (filter == null || filter.test(projectile)) {
                projectiles.add(projectile);
            }
        }
        return new LocatableEntityQueryResults<>(projectiles);
    }
}
