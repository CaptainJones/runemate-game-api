package com.runemate.game.api.osrs.entities.status;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.util.calculations.*;

public final class OSRSCombatGauge implements CombatGauge {
    private final long uid;

    private OpenHealthbar healthbar;

    public OSRSCombatGauge(final long uid) {
        this.uid = uid;
    }

    private OpenHealthbar healthbar() {
        if (healthbar == null) {
            healthbar = OpenHealthbar.create(uid);
        }
        return healthbar;
    }

    @Override
    public int getMaxWidth() {
        return healthbar().getMaxWidth();
    }

    @Override
    public int getCurrentWidth() {
        return healthbar().getCurrentWidth();
    }

    @Override
    public int getPercent() {
        return CommonMath.fractionToPercent(getCurrentWidth(), getMaxWidth());
    }

    @Override
    public boolean isValid() {
        return getPercent() != -1;
    }

    @Override
    public String toString() {
        return "StatusGauge(percent=" + getPercent() + ")";
    }
}
