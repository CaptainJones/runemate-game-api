package com.runemate.bot.test;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.framework.*;
import java.util.*;
import java.util.function.*;

/**
 * This bot can be used to test new features that you're working on.Recommend that you use this to make it obvious to the reviewer
 * how exactly the code change was tested.
 * <p>
 * The "testJar" gradle task will produce a jar containing this bot which you can place in a bot directory.
 */
public class FeatureTestBot extends LoopingBot {

    @Override
    public void onStart(final String... arguments) {
        if (this instanceof EventListener) {
            getEventDispatcher().addListener((EventListener) this);
        }
    }

    @Override
    public void onLoop() {
        final Predicate<SpriteItem> predicate = item -> {
            final ItemDefinition definition;
            return (definition = item.getDefinition()) != null && definition.isMembersOnly();
        };
        System.out.printf("anyMatch %s%n", Parallelize.anyMatch(Equipment.getItems(), predicate));
        System.out.printf("allMatch %s%n", Parallelize.allMatch(Equipment.getItems(), predicate));
        System.out.printf("noneMatch %s%n", Parallelize.noneMatch(Equipment.getItems(), predicate));
        System.out.printf("map %s%n", Parallelize.map(Equipment.getItems(), SpriteItem::getDefinition));
    }
}
